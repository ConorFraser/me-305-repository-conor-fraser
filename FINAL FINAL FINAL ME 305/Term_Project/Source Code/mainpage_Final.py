'''@file                mainpage_Final.py
   @brief               Brief for mainpage_Final.py
   @details             Details for mainpage_Final.py 

   @mainpage
     
   @section rtr_bck     To Return the to Main Portfolio...
                        <b> <a href="https://conorfraser.bitbucket.io/portfolio/html/"> ...Click Here </a> </b>
                        <br>
                        
   @section tst_rpz     ME 305 Term Project Cover Page 
                        Our Term Project consists of hardware drivers, tasks for those drivers, vector <br>
                        and share files to transmit data, and a main file to tie all parts of the code together. <br> 
                        Attached you will see the associated Task Diagram, Finite State Machine Diagram, <br>
                        and Closed Loop Controller modelled in Simulink that show the design of our interacting files. <br>
   
   @section tst_rpx     Driver List: 
                        IMU, panel, motorDriver, motor 1, motor 2, closedLoopControllerDriver <br>
                        
   @section tst_rpc     IMU driver 
                        Can read or write calibration coefficients from the IMU. <br>
                        Can be calibrated. <br>
                        Returns measured euler angles and angular velocity. <br>

                        
   @section tst_rvv     Panel Driver: 
                        Scans Z direction to see if it is being touched. <br>
                        Scans X and Y position to return coordinates for the ball location. <br>
                        Can be calibrated. <br>

                        
   @section tst_rbb     Motor Driver:
                        Creates motor objects for each motor. <br>                     
                        
   @section tst_oot     Motor 1: 
                        Sets duty cycle of motor 1. <br>
                        
   @section tst_rll     Motor 2:
                        Sets duty cycle of motor 2. <br>
                        
   @section tst_rkk     Closed Loop Controller Driver:
                        Computes the matrix math to determine gain adjusted duty cycles. <br>
                        
   @section tst_rhh     Our task list contains:
                        Task IMU, Task Panel, Task Motor Driver, Task Motor 1, Task Motor 2, Task User, and Task Closed Loop Controller. <br>
                        The tasks cooperatively run their drivers by switching between each other at a high frequency. <br>

   @section tst_rrr     Methods of transferring data: 
                        Kinematic Vector: Takes data from the IMU and Panel and sends the kinematic vector <br>
                        to the Task and Driver for the Closed Loop Controller. <br>
                        <br>
                        Gain Vector: Takes the gain values from the Task User and sends them to the Task <br>
                        and Driver for the Closed Loop Controller. <br>
                        <br>
                        Shares: Used to share data between tasks. <br>
   
   @section tst_eee     Helpful Code We Added:
                        We noticed that tuning the gains proved to be difficult. To alleviate the time it took to tune the controller, <br>
                        we implemented a system to alter the gain values up or down by 0.00005. This allows us to start at low gain values <br>
                        and then steadily increase the gains till they reach a level where closed loop speed control is exhibited. <br>
                        <br>
                        We also added a method for us to set the panel to flat and then zero its position to provide an offset to the IMU. <br>
                        This would prevent a faulty IMU from setting the board to a position where it thinks it is flat, <br>
                        but in actual fact it is at an angle. <br>
                        <br>
                        Adding a calibration method to the panel was something we completed, but never got to utilize because <br>
                        of the issues associated with the balancing of our panel. <br>

                        
   @section tst_qqq     Issues with the final product: 
                        We were unable to fully balance the panel and ball on said panel by the conclusion of the project. <br>
                        We believe our gains were never fully tuned. We also suspect our code is running slow and there <br>
                        may be issues with the calibration of the hardware. <br>





                        
                        
   @section fnl_www     Final Video
                        Video Link:
                        <br>
                        \htmlonly
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/7xuR2YwOMh8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        \endhtmlonly
                        <br>

                        
   @section fsm_ttk     FSMs and Task Diagrams
                        <br>
                        Overall Task Diagram<br>
                        \image html TermProjectTaskUserPNG.PNG
                        <br>
                        User Task FSM <br>
                        \image html TermProjectFSMPNG.PNG
                        <br>
                        Closed Loop Diagram <br>
                        \image html TPClosedLoopControllerPNG.PNG
                        <br>
                       
    
   @author              Conor Fraser

   @copyright           License Info

   @date                December 9, 2021
'''