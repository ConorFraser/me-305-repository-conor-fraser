'''@file                mainpage_HW3.py
   @brief               Response Plots for HW 0x03
   @details             Short .py code used to display response plots as .png files

   @mainpage
   
   @section rtr_bck     To Return the to Main Portfolio...
                        <b> <a href="https://thillman.bitbucket.io/portfolio/html/"> ...Click Here </a> </b>
                        <br>
                        
   
   @section sec_intro   Introduction
                        HW 0x03 is concerned with implementing the equations <br>
                        found in HW 0x02. The response plots are shown here. <br>
                        <br>
   @section hnd_clcs    Response Plots
                       \image html 21.PNG
                       \image html 22.PNG
                       \image html 23.PNG
                       \image html 24.PNG

   
   @author              Tanner Hillman

   @copyright           License Info

   @date                December 9, 2021
'''