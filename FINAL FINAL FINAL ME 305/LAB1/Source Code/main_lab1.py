''' @file       mainpage_Lab1.py
    @brief      Driver for modification of LED duty cycle
    @details    5 total states that the program cycles through. State 0
                introduces the user to the program. The first state gives instructions 
                about the remaining three states and what they achieve. The second state flickers the LED
                in a square wave pattern. The third state flickers the LED in a 
                sine wave pattern. The fourth state flickers the LED in a 
                sawtooth wave pattern. Once state three is initialized, it will
                cycle in the pattern 3, 4, 5 and back to 3 after each button 
                press (blue button on nucleo).
    @author     Conor Fraser
    @date       10/04/2021
'''

# Imports
import time
import pyb
import math
# -*- coding: utf-8 -*-

# initialize each Nucleo pin used
pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)


# introduces buttonPress as a global variable
def onButtonPressFCN(IRQ_src):
    '''@brief       Allows the blue button the nucleo to be pressed triggering a state change.
    @details     Sets the variable buttonPress to a global variable. Sets PinA5 to high, sleeps, then sets PinA5 to low. The buttonPress variable is set to True which is how the program triggers a state change.
    '''
    global buttonPress
    pinA5.high()
    time.sleep(.25)
    pinA5.low()
    buttonPress = True

#The main program should go at the bottom after function definitions
if __name__ == "__main__":
    # The state the finite state machine is about to run
    state = 0
    # The number of iterations in the FSM
    runs = 0
    # Tracks current/elapsed time
    tc = -1
    # Tracks single instance of time
    ts = -1
    # Difference between tc and ts
    td = tc - ts
    # 0-100 value that defines LED brightness level
    led = 0
   
    ButtonInt = pyb.ExtInt(pinC13,mode=pyb.ExtInt.IRQ_FALLING,
                           pull=pyb.Pin.PULL_NONE,callback=onButtonPressFCN)
    buttonPress = False
    # Initialize timing systems
    tim2 = pyb.Timer(2,freq = 20000)
    t2ch1 = tim2.channel(1,pyb.Timer.PWM,pin=pinA5)
   
    while(True):    
        try:                        # If no keyboard interrupt, run FSM
            tc = time.ticks_ms()
            td = tc - ts
            if (state == 0):
                # Run state 0 code
                print('Program initialized. Press blue button to continue for '
                      'further instructions.')
                
                if (buttonPress == True):
                    state = 1       # Transition to state 1
                    buttonPress = False
                    
            elif (state == 1):
                # Run state 1 code
                print('Press blue button again to transition to state 2: '
                      'square wave.')
                if (buttonPress == True):
                    state = 2       # Transition to state 2
                    ts = tc
                    buttonPress = False
                    
            elif (state == 2):
                # Run state 2 code
                print('State 2 initialized (square wave). Press blue button '
                      'to continue to State 3 (sine wave).')
                # Square wave LED cycle. Using floored quotient operation, 
                # results in even and odd whole numbers. The modulus ensures
                # even values are 0 and odd values are 1, therefore displaying
                # an ON state (1) and OFF state (0) that resembles a square
                # wave. Multiplied by 100 to account for LED brightness from 
                # 0 to 100.
                led = (((td) // 500) % 2) * 100
               
                if (buttonPress == True):
                    state = 3       # Transition to state 3
                    ts = tc
                    buttonPress = False
                    
            elif (state == 3):
                # Run state 3 code
                print('State 3 initialized (sine wave). Press blue button '
                      'to continue to State 4 (sawtooth wave).')
                # Sine wave LED cycle
                # 10000 ms period, added by 50 instead of 100 because sine wave
                # is to begin at the sine wave average value/midpoint.
                led = 50 * math.sin(2 * math.pi * td / 10000) + 50
                   
             
                if (buttonPress == True):
                    state = 4       # Transition to state 4
                    ts = tc
                    buttonPress = False
                    
            elif (state == 4):
                # Run state 4 code
                print('State 4 initialized (sawtooth wave). Press blue button '
                      'to continue to State 2 (square wave).')
                # Sawtooth wave LED cycle
                # td increases brightness at a constant rate while modulus 
                # restarts function when td exceeds 1000 ms
                led = ((td) % 1000) / 10
                
                if (buttonPress == True):
                    state = 2       # Transition to state 2
                    ts = tc
                    buttonPress = False
            
            # assigns variable led as pulse width integer value
            t2ch1.pulse_width_percent(led)
            # Increment run counter to track number of FSM iterations                       
            runs += 1 
                      
        # If keyboard interrupt, exit loop
        except KeyboardInterrupt:   
            break
   
    print('Program terminating')