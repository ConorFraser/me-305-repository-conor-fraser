'''@file                mainpage_HW0.py
   @brief               FSM for HW 0x00
   @details             Short .py code used to display FSM as .png files

   @mainpage
   
   @section rtr_bck     To Return the to Main Portfolio...
                        <b> <a href="https://thillman.bitbucket.io/portfolio/html/"> ...Click Here </a> </b>
                        <br>
                        

   @section sec_intro   Introduction
                        For HW 0x00 we were given the task of creating a logic table, <br>
                        and an associated Finite State Machine to model the system's <br>
                        behavior.
                        <br>
    @section FSM        FSM and Logic Table
                       \image html 25.PNG
                       \image html 26.PNG

   
   @author              Tanner Hillman

   @copyright           License Info

   @date                December 9, 2021
'''
