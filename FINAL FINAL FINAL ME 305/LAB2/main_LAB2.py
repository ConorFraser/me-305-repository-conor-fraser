# -*- coding: utf-8 -*-
"""
@file          main_LAB2.py
@brief         The main function and skeleton of the encoder control code
@details       Imports task_encoder and task_user. Sets frequency to run the task_encoder and task_user.
               Transfers parameters between files.
@author        Conor Fraser
@date          10/05/2021
"""

print('-------------program started ----------------')

import task_encoder           # imports the task encoder so functions from it can be called. Also allows the period between each time the program runs to be set
import task_user              # imports the task user so functions from it can be called. Also allows the period between each time the program runs to be set

task_encoder_period = 50   # data collection increment for the period used in the task encoder and driver. 50 milliseconds 

task_user_period = 50      # task user init period. 50 milliseconds

    
if __name__ == '__main__':   
  
    encoder_task = task_encoder.Task_Encoder(task_encoder_period)    # take the 50 ms value and puts it into the period for tunning task encoder. will run task_encoder every 100 ms
                                                                                 # creates an object for the task_encoder that allows functions from it to be called
                                                                                 
    user_task = task_user.Task_User(task_user_period)                # take the 50 ms value and puts it into the period for running the task user. will run task_user every 10 ms
                                                                                 # creates an object for the task_user that allows functions from it to be called
    
    zero = False                       # creates boolean value for zero setting it to false
     
    while(True):                       # The loop will always run because it's true. Will only stop running if false.
        
        try:                           # try catch block. connects try and except
            pos_and_delta = encoder_task.run(zero)
            zero = user_task.run(pos_and_delta)   # look at the z input line in the write function of task_user
            
        except KeyboardInterrupt:              # ctrl + c which then leads to the break command
            break                              # break is a cmd that causes the loop to stop regardless of the conditions
        
    print('Program Terminating')

    


