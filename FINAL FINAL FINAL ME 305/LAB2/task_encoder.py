# -*- coding: utf-8 -*-
"""
@file          task_encoder.py
@brief         Interfaces between the main.py function and the encoder.py function to interact with the quadrature encoders.
@details       Provides the interface for the encoder positon to be set to zero or updated through calling the encoder driver file.
               Runs cooperatively with the task_user.
@author        Conor Fraser
@date          10/05/2021
"""

import utime
import pyb
import encoder

# Instantiates the pins for the encoders
# encoder 1 pins
pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
# encoder 2 pins
pinC6 = pyb.Pin(pyb.Pin.cpu.C6)
pinC7 = pyb.Pin(pyb.Pin.cpu.C7)





class Task_Encoder:
    ''' @brief                                    An encoder task for running the encoder driver file.
        @details                                  Includes an init and run function. 
                                                  Interfaces with the task_user class and the encoder hardware.
    '''
    def __init__ (self,period):
        ''' @brief                                Instaniates the variables used in the task_encoder.
            @details                              Sets self variables to the pins needed for interaction with the encoder.   
            @param period                         The period is imported from the main.py function to establish the frequency the task_encoder should be ran at.
        '''
               
        self.period = period
        self.next_time = utime.ticks_add(utime.ticks_ms(), self.period)
        self.encoder1 = encoder.Encoder(pinB6, pinB7, 4)   
        # add the other encoder in future
        
    def run(self, zero):
        ''' @brief                                Calls the encoder driver to update after time has passed.
            @details                              Establishes if the encoder position and delta should be reset to zero or if they should be updated per the encoder's new position.
            @param zero                           The zero is imported from the main function as a boolean and allows the task_encoder 
                                                  to check whether the 'z' or 'Z' has been hit to establish whether or not the encoder should zero out or not.
            @return                               Returns the pair (0,0) if we are setting the encoder position to zero. 
                                                  Otherwise, it returns the updated encoder position and updated delta value. 
        '''
        
        if zero == True:                             # if task user sets zero to true, and then main runs and calls run with zero = true, we run thi function to set encoder to zero
            self.encoder1.set_position(0)            # set encoder position to zero
            print('setting encoder1 to zero TEST')
            
            
            return (0,0)                           # exits function and returns (0,0). Prevents the second if function running after the zero position function
            
        elif (utime.ticks_diff(utime.ticks_ms(), self.next_time) >= 0):   
                                                                          # The run function will be called continously. We want a time delay so the code can actually run. Here's how to check that enough time has passed
                                                                          # we had set the self.next_time previously. 
                                                                          # if the difference between the current time (utime.ticks_ms())
                                                                          # and the self.next_time is greater than/equal than zero then run the function.
                                                                          # if the value is greater than zero, then we must be at or past the self.next_time value we had previously set.
                                                                          # if we are past the self.next_time value we can update the encoder hrough function, otherwise we just return self.previous_update
            
            self.encoder1.update()
            # add other encoder here as well in future?
            self.next_time += self.period                                     # this is just setting the next_time. It takes the current nextime time and 
                                                                              # then adds the period that would cause us to run the encoder update again
              
        return (self.encoder1.get_position(), self.encoder1.get_delta())   # returns a pair of position and delta