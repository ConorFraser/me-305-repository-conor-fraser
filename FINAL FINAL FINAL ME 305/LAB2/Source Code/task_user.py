# -*- coding: utf-8 -*-
"""
@file          task_user.py
@brief         Handles user input and runs cooperatively with the task_encoder.py
@details       Includes a user display. 
               Utilizes a State machine.
               Provides the user an abilty to trigger the encoder to print position, print delta, zero the position, and collect data.               
               The Task_User class includes an init, run, and read function.
@author        Conor Fraser
@date          10/05/2021
"""

import pyb
import utime


S0_INIT = 0
S1_STANDBY = 1


class Task_User:
    ''' @brief                                          Contains the user interface display and commands.
        @details                                        Includes an init, run and read function.
                                                        Provides the user a way to interact with the encoders through a set of displayed commands.
    '''
    
    def __init__(self, period):
        ''' @brief                                      Instaniates object variables. 
            @details                                    Contructs a plethora of self variables that allow for hardware interaction and the creation of a FSM. 
            @param period                               The param "period" is imported from main to manage how often the code should run.
        '''
        
        self.period = period            # linked to the main function.
        
        
        self.next_time = utime.ticks_add(utime.ticks_ms(), self.period)
        
        self.state = S0_INIT             # need this to initialize the first state as an object so it starts at S0
        
        
        self.serial_port = pyb.USB_VCP()       # hardware interaction that creates a variable serial_port to interact with the hardware
        
        self.tick_list = []           # used for data collection
        
        self.position_list = []       # used for data collection
        
        self.data_collection_start_time = 0
        
        self.data_collection = False
        
        self.display_position_time = 30000 # 30 seconds
        
    def run(self, pos_and_delta):
        ''' @brief                                      Runs the state machine
            @details                                    Prints the S0 instructions for the user interface
                                                        Updates the self.next_time variable so the program understands time has 
                                                        passed and therefore this block can run again.
                                                        The period brought in earlier is the frequency that this function runs at. 
                                                        In our case it is currently set at 50ms.
            @param pos_and_delta                        This param is brought in from the task_encoder. 
                                                        It is a pair of the position and delta of the encoder.
            @return                                     Returns true if the encoder is set to a zero position. Returns false if the encoder is not set to a zero position.
            
        '''        
      
        if (utime.ticks_diff(utime.ticks_ms(), self.next_time) >= 0): # always true? self.nextime thing from aboce the special code and period and stuff
            if self.state == S0_INIT:  # put in state zero
                print('See Below Instructions for encoder commands\n'
                      '-------------------------------------------\n'
                      'z or Z: Set encoder position to zero \n'
                      'p or P: Print encoder position to PuTTY \n'
                      'd or D: Print encoder delta to PuTTY \n'
                      'g or G: Collect encoder data for 30 seconds, then print to PuTTY\n'
                      's or S: End data collection early\n'
                      'ESC: Reprint instructions\n'
                      'Ctrl C: Terminate Program\n'
                      '-------------------------------------------')
                
                self.state = S1_STANDBY # goes to next state
                self.next_time += self.period    # this is the time delay. it allows the next time to increase.
                                                 # run function is continualy being called so we need to reset self.next_time
                                                 # we use the period established in the __init__ block
                                                 # that period is from the main.py where the object is created and we set it at 50 ms
                
            elif self.state == S1_STANDBY:
                userInput = self.read()
                check_for_zero_position = self.write(userInput,pos_and_delta)    # returns True from write if setting to zero, returns False if doing something else
                
                self.next_time += self.period    # this is the time delay. it allows the next time to increase.
                                                 # run function is continualy being called so we need to reset self.next_time
                                                 # we use the period established in the __init__ block
                                                 # that period is from the main.py where the object is created and we set it at 50 ms
                if check_for_zero_position == True:
                    return check_for_zero_position
        return False
    
    def write(self,userInput,pos_and_delta):
        ''' @brief                                      This function includes the user commands to control the encoders.
            @details                                    The user interface allows for the encoders to be set to zero, position to be printed, 
                                                        the delta to be printed, the instructions to be reprinted and data to be collected and stopped.
            @param   userInput                          userInput is the users input into the PuTTY window. It controls the if statements 
                                                        in the write function which will then cause the needed actions to occur.
            @param   pos_and_delta                      pos_and_delta is brought from the task_encoder. It is a pair of the position and delta values of the encoder.    
            @return                                     Returns true if the encoder is set to a zero position. Returns false if the encoder is not set to a zero position.
        '''
        
        
        (encoder_pos_local,delta_pos_local) = pos_and_delta      # ordered pair
        current_time = utime.ticks_ms()                   # snapshots the time and assigns it to the variable current_time
        
        if userInput == b'z' or userInput == b'Z':        # for the zero input. linked to zero variable in main
            return True
        
        if self.data_collection == True:
            
            if (utime.ticks_diff(current_time, self.data_collection_start_time) > self.display_position_time) or ((userInput == b's' or userInput == b'S')):   # checks if we are past 30 seconds or if we hit s for premature data collection end
            # in the further, could've done the data_collection_start_time plus the 30 seconds to record it once and then check for a comparison with the current time and the preset finish time instead of doing a subtraction every single time the function runs. Would make the program more efficient
                print('Time [sec], Position [ticks]')
                for n in range(len(self.tick_list)):
                    print('{:}, {:}'.format(self.tick_list[n]/1000,self.position_list[n]))   # this 1000 is only for the print statement not the memory issues
                self.data_collection = False
                self.tick_list = []
                self.position_list = []
                
            else:                                                                                           # this is if we  are in data collection and we haven't hit s or past 30 seconds so continue to record data
                self.tick_list.append(utime.ticks_diff(current_time, self.data_collection_start_time))
                self.position_list.append(encoder_pos_local)                                                                                               
            
        elif userInput == b'g' or userInput == b'G':
            print('Data Collecting Intialized')
            self.data_collection = True
            self.data_collection_start_time = current_time

        elif userInput == b'd' or userInput == b'D':
            print('Delta Value = ' +str(delta_pos_local))
            
        elif userInput == b'p' or userInput == b'P':
            print('Encoder Value = ' +str(encoder_pos_local))
        
        return False
        
    
    def read(self):
        ''' @brief                                      This reads the userInput one character at a time
            @details                                    Utilizes the self.serial_port.read() command to grab the data from the VCP and PuttY
            @return                                     Returns the userInput variable to be used elsewhere in the code.
        '''
        
        userInput = b''                          # empty string of bytes for user_input
        
        if(self.serial_port.any()):
            userInput = self.serial_port.read(1)
            
            self.serial_port.read()             # TEST IF THIS NEEDS TO BE THERE
            
           
            if(userInput == b'\x1b'):           # using escape to restart the print instructions screen
                self.state = S0_INIT
                return ''
            
        return userInput