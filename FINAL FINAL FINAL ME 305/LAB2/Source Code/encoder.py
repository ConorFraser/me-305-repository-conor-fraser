# -*- coding: utf-8 -*-
"""
@file          encoder.py
@brief         A driver for reading the Quadrature Encoders
@details       Includes objects and classes for future encoding use. 
               Includes the math on preventing underflow and overflow of the encoder due to the bit limit.
               Stores the previous two values to get delta and allows the encoder to be set to zero.
@author        Conor Fraser
@date          10/05/2021
"""
import pyb

class Encoder:
    '''@brief                                               Interface with quadrature encoder.
       @detials                                             Includes 5 functions: init, update, get_position, set_position, get_delta.
                                                            Provides a framework to interact with the encoder hardware.
    '''

    
    def __init__(self, pin1, pin2, timer_number):        
        '''@brief                                           Constructs an encoder object and timers.
           @details                                         Instantiates the variables used in the Encoder Class.
                                                            Sets self. variables for the period, position, and the previous two readings of the encoder position.
           @param pin 1                                     Enables the encoder pins on channel 1.
           @param pin 2                                     Enables the encoder pins on channel 2.
           @param timer_number                              Sets the timer number. Motors will use timer 3 and encoders will use timer 8.
    
        '''
        #self.posmeas = 0
        self.period = 65536       # period max 16 bit number + 1, look at the updating delta function because it cant 65535        
        self.position = 0   
        self.previous_reading = 0
        self.current_reading  = 0
        self.delta = 0
        
        ##  Timer for CH1 and CH2 pins
        self.timer = pyb.Timer(timer_number, prescaler = 0, period = 65535)   # creates self.timer. timer_number is whatever timer is used.  #test 65536 in the lab see if any changes happen. CAN'T USE 65536 will not allow the encoders to run at all
        
        self.timer.channel(1, pyb.Timer.ENC_AB, pin=pin1)           # enables the encoder pins, sets them to channel 1        
        self.timer.channel(2, pyb.Timer.ENC_AB, pin=pin2)           # enables the encoder pins, sets them to channel 2
    
    def update(self):
        '''@brief                                           Updates encoder position and delta.
           @details                                         Records previous two positions. Calculates Delta. 
                                                            Accounts for underflow and overflow of the encoder and bit limit.
           @return                                          Returns self.position which is the updated and corrected current position of the encoder.
        '''
        # this is for creating a delta value
        self.previous_reading = self.current_reading
        self.current_reading = self.timer.counter()
        # new_position = self.timer.counter()
        self.delta = self.current_reading - self.previous_reading
        
        # corrects/updates the delta. The delta is fixed if there is an issue with overflow as per the if statement
        # or underflow as per the elif statement.         
        if(self.delta <= -self.period/2):            # where the 65536 comes into play
            self.delta += self.period
            
            
        elif(self.delta >= self.period/2):
            self.delta -= self.period
            
        # The position object is updated with the corrected delta value
        self.position += self.delta
        
        return self.position     # The function returns a value. The self.position is now updated
                       
    def get_position(self):
        '''@brief                                           Returns encoder position.
           @details                                         Relies on the update function to update the encoder position.
           @return                                          Returns the position of the encoder shaft.
        '''
        
        return self.position    # returns the updated position value see the return statement in the transtion_to function in tsak_encoder.py
    
    
    def set_position(self, pos):
        '''@brief                                           Sets position of the quadrature encoder.
           @details                                         Generally used to set the encoder postion to zero.
           @param pos                                       The new position of the encoder shaft.
        '''
        print('Setting position value')
        
        self.position = pos       # used to set the position to a value. In our case that is probably strictly zero
        
    
    def get_delta(self):
        '''@brief                                           Gets delta value of the encoder.
           @details                                         The previous two values are stored so the encoder can be set to zero when need be 
                                                            and so the delta values stay consistent when navigating bit limits.
           @return                                          The change in position of the encoder shaft between the two most recent updates.
        '''
        return self.delta        # the self.delta is updated and fixed earlier then returned.
        