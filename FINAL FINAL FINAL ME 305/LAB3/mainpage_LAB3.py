'''@file                mainpage_Lab3.py
   @brief               Brief for mainpage_Lab3.py
   @details             Details for mainpage_Lab3.py 

   @mainpage

   @section rtr_bck     To Return the to Main Portfolio...
                        <b> <a href="https://conorfraser.bitbucket.io/portfolio/html/"> ...Click Here </a> </b>
                        <br>

   @section tst_rbb     Motor Driver:
                        Creates motor objects for each motor. <br>                     
                        
   @section tst_oot     Motor 1: 
                        Sets duty cycle of motor 1. <br>
                        
   @section tst_rll     Motor 2:
                        Sets duty cycle of motor 2. <br>
                        
   @section tst_rrl     Encoder Driver:
                        Gets encoder pos, sets encoder pos to zero, gets encoder angular velocity. <br>                     

   @section sec_enc     Encoder 
                        The encoder file allows the program to interact with the encorder on the board <br>
                        
                        
   @section fnl_www     Final Video
                        Video Link:
                        <br>
                        \htmlonly
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/Rxygu1pWYa4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        \endhtmlonly
                        <br>

                        
   @section fsm_ttk     FSMs and Task Diagrams
                        <br>
                        Task Diagram<br>
                        \image html Lab 03 Task Diagram PNG.PNG
                        <br>
                        FSM <br>
                        \image html Lab 03 FSM PNG.PNG
                        <br>
                        Encoder Plots <br>
                        \image html Lab03EncoderPlots.PNG
                        <br>
                        Encoder Data Snippet <br>
                        \image html Lab03EncoderTableSnippet.PNG
                        <br>
   
   @author              Conor Fraser

   @copyright           License Info

   @date                December 9, 2021
'''