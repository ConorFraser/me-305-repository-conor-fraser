'''@file                mainpage_Lab4.py
   @brief               Brief for mainpage_Lab4.py
   @details             Details for mainpage_Lab4.py 

   @mainpage

   @section rtr_bck     To Return the to Main Portfolio...
                        <b> <a href="https://conorfraser.bitbucket.io/portfolio/html/"> ...Click Here </a> </b>
                        <br>

   @section tst_rbb     Motor Driver:
                        Creates motor objects for each motor. <br>                     
                        
   @section tst_oot     Motor 1: 
                        Sets duty cycle of motor 1. <br>
                        
   @section tst_rll     Motor 2:
                        Sets duty cycle of motor 2. <br>
                        
   @section tst_rrl     Encoder Driver:
                        Gets encoder pos, sets encoder pos to zero, gets encoder angular velocity. <br>                     

   @section sec_enc     Encoder 
                        The encoder file allows the program to interact with the encorder on the board <br>
                        
   @section tst_bbb     Closed Loop Controller:
                        Completes closed loop motor control. <br>                     
                        
   
                        
   @section fnl_www     Final Video
                        Video Link:
                        <br>
                        \htmlonly
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/tG4qmKjGVGg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        \endhtmlonly
                        <br>

                        
   @section fsm_ttk     FSMs and Task Diagrams
                        <br>
                        Task Diagram<br>
                        \image html task.PNG
                        <br>
                        FSM <br>
                        \image html fsm.PNG
                        <br>
                        Closed Loop Design <br>
                        \image html closed.PNG
                        <br>
                        Gain Tunes <br>
                        \image html graph.PNG
                        <br>
   
   @author              Conor Fraser

   @copyright           License Info

   @date                December 9, 2021
'''