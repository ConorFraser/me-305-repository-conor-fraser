# -*- coding: utf-8 -*-
''' @file                       BNO055.py
    @brief                      A driver for the 9-axis BNO055 IMU.
    @details                    Contains a BNO055 class which includes an init function and 6 functions to interact with the IMU.
                                The change_operation_mode function changes the operation mode of the IMU to get vs write data to the IMU.
                                The get_calibration_status function returns the calibration status of the IMU.
                                The get_calibration_coefficient function returns the calibration coefficients of the IMU.
                                The write_calibration_coefficient functions writes calibration coefficients to the IMU.
                                The read_euler_angles function returns the euler angles from the IMU.
                                The read_angular_velocity function returns the angular velocity data from the IMU.                           
    @author                     Jason Davis
    @author                     Conor Fraser
    @date                       October 11, 2021
'''


import struct

#BELOW ARE SOME NOTES

# pb8 and pb9 for the i2c 1
# pb13 and pb14 for i2c 2



# i2c  = I2C(1, I2C.MASTER)       
# i2c.mem_read(4, 0x28, 0)  (data, address, mem address )
# returns b'\xa0\xfb2\x0f'

# bytes_in = i2c.mem_read(4, 0x28, 0)
# bytes_in[0]
# returns 160

# hex(bytes_in(0])
# returns '0xfb'
     
# hex(bytes_in(2])
# returns '0x32'
        

# buf = bytearray(4)
# buf
# returns bytearray(b'\x00\x00\x00\x00')
       
# bytes_in = i2c.mem_read(buf, 0x28, 0) 
# buf
# returns bytearray()       

# END OF NOTES

class BNO055:
    '''@brief                                           Establishes a driver for the BNO055 IMU
       @details                                         Includes the following functions: init, change_operation_mode, get_calibration_status, 
                                                        get_calibration_coefficient, read_euler_angles, and read_angular_velocity
    '''
    
    def __init__(self, address, i2c):
        '''@brief                                       Init function establishes our self variables
           @details                                     Creates self objects for our device address, I2C setup and changes the IMU operation mode 
                                                        to allow for calibration and euler angle use
           @param address                               Brings the IMU device address of 0x28 from the main function to the BNO055 file and class
           @param i2c                                   Brings the established I2C master with baudrate into our driver
        '''
    # pyb.I2C object is brought in. It will default to master
    # i2c  = I2C(1, I2C.MASTER) brought from main
    
        self.address = address   # device address, 0x28
        self.i2c  = i2c          # i2c device object
           
        self.change_operation_mode(0x0C)   # 12   needed to make sure we are in the correct operating mode to calibrate then spit out euler angles
       
    def change_operation_mode(self, data):
        '''@brief                                       Changes operation mode of the BNO055
           @details                                     Modes include calibration mode and outputing data mode. 
           @param data                                  Brings in the 0x0C from out init function to be used in our i2c.mem_write command        
        '''
    # method to change operating mode from master to slave
    # import mode variable that includes SLAVE most likely
        self.i2c.mem_write(data, self.address, 0x3D)
        
    def get_calibration_status(self):
        '''@brief                                       Retrieves the calibration data from the IMU
           @details                                     Utilizes bit shifting to read through the IMU data. 
                                                        A byte_buffer is used to signify a bytearray of 1.
                                                        This bytearray is brought into our mem read command and 
                                                        we use the 0x35 address to retrieve the calibration data from the IMU
                                                        We then bit shift into our cal_status array.
           @return                                      Returns the cal_status array which includes the updated calibration values from our IMU
        '''
    # retrieve calibration status
    # page 67 on the bno055 data sheet
    # 4.3.54 calib_stat     0x35 <- is that the hexadec number for calling the calibration status
        byte_buffer = bytearray(1)
        cal_bytes = self.i2c.mem_read(byte_buffer, self.address, 0x35)       # reading the imu output. needs device address of 0x28 and the 0x35 address specifically for calibration
        # print("Binary:", '{:#010b}'.format(cal_bytes[0])        # the array is [system, gyro, accel, magnet] the values will fluctuate from 0,1,2,3
        cal_status = ( cal_bytes[0] & 0b11,                       # reads the LSB and MSB for the four array values. 
                      (cal_bytes[0] & 0b11 << 2) >> 2,            # shifts two bits down to the next
                      (cal_bytes[0] & 0b11 << 4) >> 4,            # shifts two bits down to the next
                      (cal_bytes[0] & 0b11 << 6) >> 6)

        #print("Values:", cal_status)
        #print('\n')
        #print('test  that get_calibration status is ran')
        return cal_status
       
    def get_calibration_coefficient(self):
        '''@brief                                       Retrieves the calibration coefficient from the IMU
           @details                                     We use a bytearray of size 22 to pull the 22 bytes associated with the 22 locations 
                                                        between the address 0x55 and 0x5A on the BNO055 Data sheet.
                                                        We take these coeffiecents and use the built in library struct to 
                                                        unpack the data and then recombine into a tuple.
           @return                                      Returns the recombined tuple of binary coefficients which is sent to back to our main function.
        '''
    # retrieve calibration coefficient from the IMU as binary data
        byte_buffer = bytearray(22)                    # there are 22 locations, from 0x55 to 0x5A
        self.i2c.mem_write(byte_buffer, self.address, 0x55)
        return tuple(struct.unpack('<hhhhhhhhhhh',byte_buffer))     # need to unpack 22 bytes so 11 sets of bytes hence the 11 h's
    
    def write_calibration_coefficient(self, data):
        '''@brief                                       Writes the calibration data back onto the IMU
           @details                                     Allows us to preset a calibration status for a future lab
           @param data                                  Brings in an address to set calibration coefficients
        '''
    # write calibration coefficients to the IMU from pre-recorded binary data.   
        self.i2c.mem_write(data,self.addr, 0x55)
        
    def read_euler_angles(self):
        '''@brief                                       Retrieves euler angles from the IMU
           @details                                     A bytearray of 6 is used to extract the two bytes needed to represent each euler angle.
                                                        The unpack function from the struct library is used to fix the signs of the 
                                                        euler angles and then return the values as a tuple.
           @return                                      Returns the recombined tuple of euler angles which is sent to back to our main function.
        '''
    # read Euler angles from the IMU to use as state measurements.
        eul_buffer = bytearray(6)
        self.i2c.mem_read(eul_buffer, self.address, 0x1A)    # queue, first in first out. thats why you don't want to do this 6 times. only need once to read the popped value over and over again
        eul_signed_ints = struct.unpack('<hhh', eul_buffer)
        # print('Unpacked Euler Angle: ', eul_signed_ints)
        eul_vals = tuple(eul_int/16 for eul_int in eul_signed_ints)
        # print('\n')
        # print('Scaled Euler Angle: ', eul_vals)
        # print('\n')
        # print('\n')
        return eul_vals
        
    def read_angular_velocity(self):
        '''@brief                                       Retrieves the angular velocity readings from the IMU
           @details                                     We use a bytearray of size 6 to extract the two bytes needed to represent each angular velocity.
                                                        The unpack function from the struct library is used to fix the signs 
                                                        of the angular velocity and then return the values as a tuple.
           @return                                      Returns the recombined tuple of angular velocity which is sent to back to our main function.
        '''
    # read angular velocity from the IMU to use as state measurements
        angular_velocity_buffer = bytearray(6)
        self.i2c.mem_read(angular_velocity_buffer, self.address, 0x14)
        angular_velocity_signed_ints = struct.unpack('<hhh', angular_velocity_buffer)
        # print('Unpacked Angular Velocity: ', angular_velocity_signed_ints)
        # print('\n')
        angular_velocity_vals = tuple(angular_velocity_int/16 for angular_velocity_int in angular_velocity_signed_ints)
        # print('Scaled Angular Velocity: ', angular_velocity_vals)
        # print('\n')
        # print('\n')
        return angular_velocity_vals
    
    