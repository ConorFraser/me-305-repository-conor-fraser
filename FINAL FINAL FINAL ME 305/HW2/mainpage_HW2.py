'''@file                mainpage_HW2.py
   @brief               Hand Calculations for HW 0x02
   @details             Short .py code used to display my completed hand calculations as .png files

   @mainpage

   @section rtr_bck     To Return the to Main Portfolio...
                        <b> <a href="https://thillman.bitbucket.io/portfolio/html/"> ...Click Here </a> </b>
                        <br>
                        
   @section sec_intro   Introduction
                        HW 0x02 is concerned with creating a simplfied model of the balancing platform <br>
                        we will use for our term project. The supplemental hand calculations are shown here <br>
                        <br>
   @section hnd_clcs    Hand Calculations
                       \image html 1.PNG
                       \image html 2.PNG
                       \image html 3.PNG
                       \image html 4.PNG
                       \image html 5.PNG
                       \image html 6.PNG
                       \image html 7.PNG
                       \image html 8.PNG
                       \image html 9.PNG
                       \image html 10.PNG
                       \image html 11.PNG
                       \image html 12.PNG
                       \image html 13.PNG
                       \image html 14.PNG
                       \image html 15.PNG
                       \image html 16.PNG
                       \image html 17.PNG
                       \image html 18.PNG
                       \image html 19.PNG
                       \image html 20.PNG
           
           
   @author              Tanner Hillman

   @copyright           License Info

   @date                December 9, 2021
'''