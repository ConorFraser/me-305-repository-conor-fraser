# -*- coding: utf-8 -*-
"""
Created on Tue Sep 28 15:20:59 2021

@author: conor

Lab 01 Hardware Intro
"""

#all imports at top
import time
import math
import pyb

pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
pinA5 = pyb.Pin (pyb.Pin.cpu.A5)

def onButtonPressFCN(IRQ_src):
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
    global buttonPress
    pinA5.high()
    time.sleep(.25)
    pinA5.low()
    buttonPress = True
    



# goes State 0 then state 1 then S2 then S3 then S4 then back to S2 S3 S4 in an endless loop

#The main program should go at the bottom after function defintions
if __name__ == '__main__':
    
    state = 0
    
    runs = 0             # number of iterations through the finite state machine
    
    
    buttonPress = False
    
    
    while(True):
        try:                        #if no keyboard interrupt, run FSM
            if (state == 0):
                #run state 0 code
                print('In state 0: Initilization')
                
                state = 1 #transition to next state which is state 1
                
#                if buttonPress == False:
#                   state = 1 #transition to next state which is state 1
#                else:
#                    pass
            
            elif (state == 1):
                #run state 1 code
                
                tState1 = 1
                while tState1 <= 1:
                    print('In state 1: Instructions')
                    print('Pressing the blue button will cycle the the LEDs flashing state.' 
                          'The first state is a square wave, the second is a sin wave, the third is a sawtooth wave.'
                          'The third state will switch to the first when the button is pressed')
                    
                    print('Avoid using rapid or long inputs to the blue button')
                    print('Press the black button labelled reset if you would like to restart')
                          
                    print('Press the blue buton to begin the cycle at the square wave')
                    tState1 += 1
                else:
                    pass
               
                if buttonPress == True:
                    state = 2 #transition to next state which is state 2
                    buttonPress = False
                else:
                    pass
            
            elif (state == 2):
                #run state 2 code
                print('In state 2: Square Wave')
                
                tSquare = 0
                while tSquare <= 10:
                    pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
                    pinA5.high()
                    time.sleep(.5)
                    pinA5.low()
                    time.sleep(.5)
                    tSquare += 1
                else:
                    print('Ten cycles of Square Wave Completed. Press Blue button to continue to Sine Wave')
                    pass
                
                if buttonPress == True:
                    state = 3 #transition to next state which is state 3
                    buttonPress = False
                else:
                    pass
            
            elif (state == 3):
                #run state 3 code
                print('In state 3: Sine Wave')
                
                #use import math, use a variable for pulsewidth, link the variable to sine function
                tSine = 0
                
                
                while tSine <= 1:
                    pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
                    tim2 = pyb.Timer(2, freq = 20000)
                    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
                    
                
                    x = math.sin(tSine)
                    
                    
                    t2ch1.pulse_width_percent(x)
                    
                    
                    
                    
                    tSine += .001
                # use three while loops. sin .5 to 1, then from 1 to 0, then from 0 to .5
                # while loops only go in one direction because of the innequality
                
                
                
                if buttonPress == True:
                    state = 4 #transition to next state which is state 4
                    buttonPress = False
                else:
                    print('One cycle of a Sine Wave Completed. Press Blue button to continue to Sawtooth Wave')
                    pass
            
            elif (state == 4):
                #run state 4 code
                print('In state 4: Sawtooth Wave')
                
                if buttonPress == True:
                    state = 2 #transition to next state which is state 2
                    buttonPress = False
                else:
                    print('Ten cycles of Sawtooth Wave Completed. Press Blue button to continue to Square Wave')
                    pass
            
            time.sleep(1)    #slow down the FSM so we can actually read console output
            
            runs += 1       #increment run counter to track number of FSM iteration
        
        except KeyboardInterrupt:                   # if keyboard interrupt, exit loop
            break
        
print('Program Terminating')
print('The number of runs = ', runs)