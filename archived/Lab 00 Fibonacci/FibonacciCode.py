# -*- coding: utf-8 -*-
"""
Conor Fraser
ME 305 LAB
Fibonacci Sequence Generator
Due 9/28/21
@FibonacciCode.py
"""

def fib(idx):
    
    fibArray = [0, 1]
    
    for i in range(2, idx+1):   #range 2 is for two number. the idx+ 1 is so the last number is incuded aka the second number
        fibArray.append(fibArray[i-1] + fibArray[i-2])
    return fibArray[idx]
'''
understanding how array and append work
i = 1 array = [0, 1]
i = 2 array = [0, 1, (1+ 0)]
i = 3 array = [0, 1, (1+0), (1 + 1)]
i = 4 array = [0, 1, 1, 2, (2+1)]




'''

# the while loop will run until it gets q. check first for q. then make sure it is integer. then check to make sure non negative. then it must be a positive integer which works as an index


if __name__ == '__main__':

    indexString = input('Please enter a fibonacci index or q to quit: ')
    
    
    while indexString != 'q':             # it is not q
        if not indexString.isnumeric():   # checks if input is a number
            indexString = input('Please enter a non negative integer: ')
            continue     #skips rest of the loop and goes back to the top of the while loop
            
        idx = int(indexString)   # setting input to be an integer because we just established it can't be anything but a number
        
        if idx < 0:
            indexString = input('Please enter a non negative integer: ')
            continue
        
        print ('Fibonacci number at index {:} is {:}.'.format(idx,fib(idx)))
        
        indexString = input('Please enter a fibonacci index or q to quit: ')    
    
    