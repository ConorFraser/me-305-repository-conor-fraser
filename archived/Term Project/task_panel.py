"""
@file       task_panel.py
@brief      Responsible for running the tasks related to reading from our touch panel.
@details    When the tasks are run, we are able to get the position of the 
            ball in x and y directions and use an alpha beta filter to 
            determine the velocity. We then share this information in a kinematic
            vector matrix that can be shared between files. 
@author:    Jason Davis
@author:    Conor Fraser
@author:    Solie Grantham
@author:    Zachary Stednitz
@date:      December 8th, 2021
"""
import os
import panelDriver
from ulab import numpy
import utime, pyb, gc, vector
from micropython import const

S0_init = const(0)
S1_calibratePanel = const(1)
S2_readCoords = const(2)
S3_checkForCalibFile = const(3)
S4_writeToCalibFile = const(4)

class Task_Panel:
    
    def __init__(self, taskID, devices, period, shares, kinVector, dbg = False):
        ''' @brief          Constructor for Task_Panel class
            @param          panelDriverObject A panel object we can use to calculate ball position
        '''
        self.taskID = taskID        
        self.panelDriverObject = devices[5]        
        self.period = period        
        self.panel_share = shares[5]        
        self.kinVector = kinVector
        self.dbg = dbg
        
        self.next_time = utime.ticks_add(utime.ticks_us(), self.period)
        self.state = S0_init
        
        self.manualCalibMatrix = []
        
    def run(self):
        ''' @brief      Runs the tasks related to the panel.
            @details    This function gets the postion of the ball and velocity in both
                        the x and y directions. This is then placed in the kinemeatic 
                        vector to be shared among the rest of the program. 
            @param      kinVector Vector that stores the position and velocity of the ball. 
        '''
        # self.kinVector = kinVector
        action = self.panel_share.read()
        current_time = utime.ticks_us()
        
        
        if (utime.ticks_diff(current_time, self.next_time) >= 0):
            if (self.state == S0_init):
                
                if (action == 0):    # first calibrate center [0,0]
                    self.transition_to(S1_calibratePanel)
                    # self.manualCalibration()
                    self.calibrate()
                    #object to call the manCalib function, ex: self.getPanelOrientation
                    self.controller_share.write(None)
                    self.transition_to(S0_init)
                    
                # if (action == 1):    # second calibrate [80,40]
                #     self.transition_to(S1_calibratePanel)
                #     self.controller_share.write(None)
                #     self.transition_to(S0_init)
                    
                # if (action == 2):     # third calibrate [80,-40]
                #     self.transition_to(S1_calibratePanel)
                #     self.controller_share.write(None)
                #     self.transition_to(S0_init)
                    
                # if (action == 3):     # second calibrate [-80,-40]
                #     self.transition_to(S1_calibratePanel)
                #     self.controller_share.write(None)
                #     self.transition_to(S0_init)
                    
                # if (action == 4):     # second calibrate [-80,40]
                #     self.transition_to(S1_calibratePanel)
                #     self.controller_share.write(None)
                #     self.transition_to(S0_init)
                    
                if (action == 5):     # read coordinates
                    self.transtion_to(S2_readCoords)
                    self.controller_share.write(None)
                    self.transition_to(S0_init)
                    
                if (action == 6):
                    self.transtion_to(S3_checkForCalibFile)
                    self.controller_share.write(None)
                    self.transition_to(S0_init)
                    
                if (action == 7):
                    self.transtion_to(S4_writeToCalibFile)
                    self.controller_share.write(None)
                    self.transition_to(S0_init)
                    
                
                    
                
                
                
                
                
                
                
                
        
        #while (True):
            if (self.panelDriverObject.panelTouch()):
                # Read x y z coordinates
                xCoordinate, yCoordinate, zCoordinate = self.panelDriverObject.readXYZ()
                # Read the alpha beta filter values.
                x_filter, x_dot_filter, y_filter, y_dot_filter, z = self.panelDriverObject.filtering(utime.ticks_us(50))
                x1 = xCoordinate
                x_dot1 = x_dot_filter
                y1 = yCoordinate
                y_dot1 = y_dot_filter
                # print('The panel is being touched: The X coordinate is {:} and the Y coordinate is {:}'.format(xCoordinate, yCoordinate))
                # Assign the values read from above to the kinematic vector to be
                # used in the main file. 
                self.kinVector.setX(x1)
                self.kinVector.setY(y1)
                self.kinVector.setXDot(x_dot1)
                self.kinVector.setYDot(y_dot1)
            else:
                print("Touch the panel")
                
                
    # def manualCalibration(self, locationToCalib):
    #     pass
        # self.locationToCalib = locationToCalib
        
        # # this might be better to do in a for loop that accepts a true or false input and then appends it
        
        # if (self.panelDriverObject.panelTouch()):
            
        #     for i in self.manualCalibMatrix:
        #         x = panelDriver.panelDriver.scanX()
        #         y = panelDriver.panelDriver.scanY()
        #         tempCoord = (x,y)
        #         self.manualCalibMatrix.append(tempCoord)
             
        # if (self.panelDriverObject.panelTouch()):
            
        #     if self.locationToCalib == [0,0]:   
                
        #     if self.locationToCalib == [80,40]:
                
        #     if self.locationToCalib == [80,-40]:
                
        #     if self.locationToCalib == [-80,-40]:
                
        #     if self.locationToCalib == [-80,40]:
          
    def calibrate(self):
        ''' @brief      Checks if a calibration file is present, and manually calibrates if not. 
            @details    When manually calibrating the panel, there are pre defined calibration
                        points that can be used to calibrate the panel. The user is prompted to 
                        tap them and then we write the calibration coefficients to the 
                        nucleo in a text file so we don't need to calibrate every time. 
        '''
        
        filename = "RT_cal_coeffs.txt"

        if filename in os.listdir():
            with open(filename, 'r') as f:
                cal_data_string = f.readline()
                cal_values = [float(cal_value) for cal_value in cal_data_string.strip().split(',')]
        else:
            # tapPoints = numpy.array([[-80, -40], [80, -40], [-80, 40], [80, 40], [0, 0]])
            # uncalPoints = numpy.zeros(5,2)
            # for i in range(5):
            #     print("Begin manual calibration...")
            #     print("Press point ({}, {})".format(tapPoints[i,0], tapPoints[i,1]))
            #     while (True):
            #         try:
            #             if (self.panelDriverObject.panelTouch()):
            #                 xCoordinate, yCoordinate, zCoordinate = self.panelDriverObject.readXYZ()
            #                 uncalPoints[i,0] = xCoordinate
            #                 uncalPoints[i,1] = yCoordinate
            #                 uncalPoints[i,2] = 1
            #                 break
            #         except:
            #             KeyboardInterrupt()
            sensorADC = []
      
        
            prompts = ['Upper Left',        # -80, 40
                       'Upper Right',       #  80, 40
                       'Lower Right',
                       'Lower Left',
                       'Center']
            XYcoords = [[-80, 40],
                      [ 80, 40],
                      [ 80,-40],
                      [-80,-40],
                      [0,0]]
            
            for coord,prompt in zip(XYcoords,prompts):
                print(f"Please press {prompts} at {XYcoords}")
                while not panelDriverObject.panelTouch():
                    pass
                sensorADC.append([self.panelDriverObject.scanX(), self.panelDriverObject.scanY(), 1])
                
                                
                # XYsensor.append(coord)
            print(sensorADC)
            print(XYcoords)
            

            return sensorADC
                        
            with open(filename, 'w+') as f:
                # calibrate manually, write coeff. to file
                (Kxx, Kxy, Kyx, Kyy, Xc, Yc) = panelDriver.calibSensorOnPanel(sensorADC)
                f.write(f"{Kxx}, {Kxy}, {Kyx}, {Kyy}, {Xc}, {Yc}\r\n")
                
if __name__ == '__main__' :
    
     Xp = pyb.Pin.board.PA7
     Xm = pyb.Pin.board.PA1
     Yp = pyb.Pin.board.PA6
     Ym = pyb.Pin.board.PA0
     xDim = 176
     yDim = 100
    
     inputPins = [Xp, Xm, Yp, Ym]    
     panelDims = [xDim, yDim]
    
     panelDriverObject = panelDriver.panelDriver(inputPins, panelDims)
     Task_PanelObject = Task_Panel(panelDriverObject)
     Task_PanelObject.calibrate()
    
     while (True):
        try:
            if (panelDriverObject.panelTouch()):
                xCoordinate = panelDriverObject.scanX()
                yCoordinate = panelDriverObject.scanY()
                sensorADC = panelDriverObject.calibSensorOnPanel()
                print('The panel is being touched: The X coordinate is {:} and the Y coordinate is {:}'.format(xCoordinate, yCoordinate))
                utime.sleep_ms(1000)
            else:
                pass
        
        except KeyboardInterrupt:
            break
     print('\n*** Program ending, have a nice day! ***\n')