'''
@file:      vector.py
@brief      Stores a vector of position and velocity values for motor control.
@details    Storing these values in a matrix makes them easily shareable between tasks when 
            trying to tune our duty cycle. This file allows us to easily modify each
            value just once and it will update everywhere to be used in
            calculating our duty cycles and matrix equations for torque. Sort of acts
            as a share, but instead of a single value we store a vector. 

@author:    Jason Davis
@author:    Conor Fraser
@author:    Solie Grantham
@author:    Zachary Stednitz
@date:      December 8th, 2021
'''

class KinematicVector:
    
    def __init__(self):
        ''' @brief      Initializes each variable and an error offset for calibration.
        '''
        self.x = -1
        self.y = -1
        self.xDot = -1
        self.yDot = -1
        
        self.thetaX = -1
        self.thetaY = -1
        self.omegaX = -1
        self.omegaY = -1
        
        self.errThetaX = float(0)
        self.errThetaY = float(0)
        self.errOmegaX = float(0)
        self.errOmegaY = float(0)
        
    def getX(self):
        ''' @brief Returns the x position of the ball.
        '''
        return self.x
    
    def getY(self):
        ''' @brief Returns the y position of the ball.
        '''
        return self.y
    
    def getXDot(self):
        ''' @brief Returns the x velocity of the ball.
        '''
        return self.xDot
    
    def getYDot(self):
        ''' @brief Returns the y velocity of the ball.
        '''
        return self.yDot
    
    def getThetaX(self):
        ''' @brief Returns the angle of tilt about the x axis of the platform.
        '''
        return self.thetaX
    
    def getThetaY(self):
        ''' @brief Returns the angle of tilt about the y axis of the platform.
        '''
        return self.thetaY
    
    def getOmegaX(self):
        ''' @brief Returns the anglular velocity about the x axis of the platform.
        '''
        return self.omegaX
    
    def getOmegaY(self):
        ''' @brief Returns the anglular velocity about the y axis of the platform.
        '''
        return self.omegaY
    
    def setX(self, value):
        ''' @brief Sets the x position to a desired value.
            @param value The desired value of x.
        '''
        self.x = value
        
    def setY(self, value):
        ''' @brief Sets the y position to a desired value.
            @param value The desired value of y.
        '''
        self.y = value
        
    def setXDot(self, value):
        ''' @brief Sets the x velocity to a desired value.
            @param value The desired value of xDot.
        '''
        self.xDot = value
        
    def setYDot(self, value):
        ''' @brief Sets the y velocity to a desired value.
            @param value The desired value of yDot.
        '''
        self.yDot = value
    
    def setThetaX(self, value):
        ''' @brief Sets the angle of tilt about the x axis to a desired value.
            @param value The desired value of ThetaX.
        '''
        value -= self.errThetaX
        self.thetaX = value
    
    def setThetaY(self, value):
        ''' @brief Sets the angle of tilt about the y axis to a desired value.
            @param value The desired value of ThetaY.
        '''
        value -= self.errThetaY
        self.thetaY = value
        
    def setOmegaX(self, value):
        ''' @brief Sets the angular velocity about the x axis to a desired value.
            @param value The desired value of OmegaX.
        '''
        value -= self.errOmegaX
        self.omegaX = value
        
    def setOmegaY(self, value):
        ''' @brief Sets the angular velocity about the y axis to a desired value.
            @param value The desired value of OmegaY.
        '''
        value -= self.errOmegaY
        self.omegaY = value
        
    def setErrThetaX(self, value):
        ''' @brief Sets value of offset to eliminate error in the readings.
            @param value Value of the error offset.
        '''
        self.errThetaX = value
        
    def setErrThetaY(self, value):
        ''' @brief Sets value of offset to eliminate error in the readings.
            @param value Value of the error offset.
        '''
        self.errThetaY = value
        
    def setErrOmegaX(self, value):
        ''' @brief Sets value of offset to eliminate error in the readings.
            @param value Value of the error offset.
        '''
        self.errOmegaX = value
        
    def setErrOmegaY(self, value):
        ''' @brief Sets value of offset to eliminate error in the readings.
            @param value Value of the error offset.
        '''
        self.errOmegaY = value