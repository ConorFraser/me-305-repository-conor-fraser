# -*- coding: utf-8 -*-
"""


@author: conor
"""

import utime
import pyb
import encoder


pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
pinC6 = pyb.Pin(pyb.Pin.cpu.C6)
pinC7 = pyb.Pin(pyb.Pin.cpu.C7)



S0_INIT = 0
S1_STANDBY = 0



class Task_Encoder:
    ''' @brief
    '''
    def __init__ (self,period):
        ''' @brief
        '''
        #below four might not be needed, just in case
        #self.enc_pos = enc_pos
        #self.delta_pos = delta_pos
        #self.enc_zero = enc_zero        
        self.period = period
        
        #defnitely need these
        self.previous_update = (0,0)
        self.next_time = utime.ticks_add(utime.ticks_ms(), self.period)
        self.encoderA = encoder.Encoder(pinB6, pinB7, 4)
        #self.encode = encoder.Encoder(timNum)
        
        
        #set state
        self.state = S0_INIT
        
    def run(self, zero):
        ''' @brief
        '''
        
        if zero:
            self.encoderA.set_position(0)
            
        if (utime.ticks_diff(utime.ticks_ms(), self.next_time) >= 0):
            
            self.encoderA.update()
            self.previous_update = self.transition_to()
            
        return self.previous_update
    
    def transition_to(self):
        self.next_time += self.period
        
        return (self.encoderA.get_position(), self.encoderA.get_delta())
            
        