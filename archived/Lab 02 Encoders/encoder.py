# -*- coding: utf-8 -*-
"""
Created on Tue Oct 12 15:34:30 2021

@file          encoder.py
@brief         A driver for reading the Quadrature Encoders
@details       Includes objects and classes for future encoding use
@author        Conor Fraser
@author        Cameron Wong
@date          10/05/2021

"""
import pyb

class Encoder:
    '''@brief       Interface with quadrature encoder
    
    '''

    
    def __init__(self, pin1, pin2, tim):
        
        '''@brief       Constructs an encoder object
    
        '''
        #self.posmeas = 0
        self.period = 65536
        self.position = 0
        self.posA = 0    
        self.posB = 0
        self.delta = 0
        

        ##  Timer for CH1 and CH2 pins
        self.timer = pyb.Timer(tim, prescaler = 0, period = 65535)
        
        self.timer.channel(1, pyb.Timer.ENC_AB, pin=pin1)
        
        self.timer.channel(2, pyb.Timer.ENC_AB, pin=pin2)
    
    def update(self):
        '''@brief       Updates encoder position and delta
        
        '''
        self.posA = self.posB
        self.posB = self.timer.counter()
        self.delta = self.get_delta()
        
        if(self.delta <= -self.period/2):
            self.position += (self.delta + self.period)
        elif(self.delta >= self.period/2):
            self.position += (self.delta - self.period)
        else: 
            self.position += self.delta
        return self.position
        
               
    def get_position(self):
        '''@brief       Returns encoder position
            @details
            @return     The position of the encoder shaft
        '''
        
        return self.position
    
    
    def set_position(self, pos):
        '''@brief       Interface with quadrature encoder
           @details
           @param position   The new position of the encoder shaft
        '''
        print('Setting position value')
        self.position = pos
        
    
    def get_delta(self):
        '''@brief       Interface with quadrature encoder
            @details
            @return     The change in position of the encoder shaft between
                        the two most recent updates
        '''
        return(self.posB - self.posA)
        