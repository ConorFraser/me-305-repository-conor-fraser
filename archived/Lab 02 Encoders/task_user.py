# -*- coding: utf-8 -*-
"""

@author: conor
"""

import pyb
import utime
import task_encoder

S0_INIT = 0
S1_STANDBY = 1


class Task_User:
    ''' @brief
    '''
    
    def __init__(self, period):
        ''' @brief
        '''
        # these 4 below self.variable = variable things do what? link data right?
        self.period = period 
        #self.encoder_pos = encoder_pos
        #self.delta_pos = delta_pos
        #self.encoder_zero = encoder_zero
        
        # this utime thing i believe is about button presses?
        self.next_time = utime.ticks_add(utime.ticks_ms(), self.period) #could be as simple as period +utime.ticks_ms()
        
        # need this to initialize the first state? this is done in the first couple if statment lines anyways?
        
        self.state = S0_INIT
        
        # hardware syntax regarding interaction with the board?
        self.serial_port = pyb.USB_VCP()
        
        self.tick_list = []
        
        self.position_list = []
        
        self.to = 0
        
        self.view = False
        
        self.display_position_time = 30100
        
    def run(self, update):
        ''' @brief
        '''
        
        
        
        current_time = utime.ticks_ms()
        
        if (utime.ticks_diff(current_time, self.next_time) >= 0): # always true? self.nextime thing from aboce the special code and period and stuff
            if self.state == S0_INIT:  # put in state zero
                print('See Below Instructions for encoder commands\n'
                      '-------------------------------------------\n'
                      'z or Z: Set encoder position to zero \n'
                      'p or P: Print encoder position to PuTTY \n'
                      'd or D: Print encoder delta to PuTTY \n'
                      'g or G: Collect encoder data for 30 seconds, then print to PuTTY\n'
                      's or S: End data collection early\n'
                      'Ctrl C: Terminate Program\n'
                      '-------------------------------------------')
                
                
                
                
                
                self.next_state(S1_STANDBY) # goes to next state
                
            elif self.state == S1_STANDBY:
                
                userInput = self.read()
                userInputValue = self.write(userInput,update)
                return userInputValue
                
        return False
    
    def write(self,userInput,update):
        ''' @brief
        '''
        
        
        (encoder_pos_local,delta_pos_local) = update
        current_time = utime.ticks_ms()
        
        if userInput == b'z' or userInput == b'Z':
            return True
        
#        if userInput == b'p' or userInput == b'P':
#            return True
#        
#        if userInput == b'd' or userInput == b'D':
#            return True
#        
#        if userInput == b'g' or userInput == b'G':
#            return True
#        
#        if userInput == b's' or userInput == b'S':
#            return True
            
        
        
        if (self.view == True and (userInput == b's' or userInput == b'S' or utime.ticks_diff(utime.ticks_add(self.to, self.display_position_time), current_time) <= 0)):
            print('Time [sec], Position [ticks]')
            for n in range(len(self.tick_list)):
                print('{:}, {:}'.format(self.tick_list[n]/1000,self.position_list[n]))
            self.view = False
            self.tick_list = []
            self.position_list = []
            
        elif(self.view):
            self.tick_list.append(utime.ticks_diff(current_time, self.to))
            self.position_list.append(encoder_pos_local)
            
        elif userInput == b'g' or userInput == b'G':
            print('Data Collecting Intialized')
            self.view = True
            self.to = current_time

        elif userInput == b'd' or userInput == b'D':
            print('Delta Value = ' +str(delta_pos_local))
            
        elif userInput == b'p' or userInput == b'P':
            print('Encoder Value = ' +str(encoder_pos_local))
            
        return False
    
    def read(self):
        ''' @brief
        '''
        
        userInput = b''
        
        if(self.serial_port.any()):
            userInput = self.serial_port.read(1)
            
            self.serial_port.read()
            
            if(userInput == b'\x1b'):
                self.next_state(S0_INIT)
                return ''
            
        return userInput
    
    def next_state(self, move_to_new_state):
        ''' @brief
        '''
        
        self.state = move_to_new_state
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                