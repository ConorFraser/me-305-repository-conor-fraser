# -*- coding: utf-8 -*-
"""


@author: conor
"""

print('-------------program started test----------------')

import task_encoder
import task_user

task_encoder_value = 2 # why 2 and 10???

task_user_value = 10 # why 2 and 10???

    
if __name__ == '__main__':   
  
    encoder_task_placeholder = task_encoder.Task_Encoder(task_encoder_value)
    
    user_task_placeholder = task_user.Task_User(task_user_value)
    
    zero = False
    
    while(True):
        
        try:
            update = encoder_task_placeholder.run(zero)
            zero = user_task_placeholder.run(update)
        
        except KeyboardInterrupt:
            break
        
    print('Program Terminating')

    


