# -*- coding: utf-8 -*-
"""
Created on Tue Oct 19 15:55:06 2021

@author: conor
"""

import time
import pyb







""""
pinA5 = pyb.Pin (pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
pinA5.high()
pinA5.low()



pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
tim2 = pyb.Timer(2, freq = 20000)
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
t2ch1.pulse_width_percent(0)
t2ch1.pulse_width_percent(50)
t2ch1.pulse_width_percent(100)

"""


# trying to get the motors to run

print('start')

nSleep = pyb.Pin (pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)         # creates nSleep pin. need the pyb.Pin.OUT_PP because this shows that an output is produced by the pin
nSleep.high()                               # hopefully this sets nSleep to high

print('sleep command completed')
#  nFault = pyb.Pin (pyb.Pin.cpu.B2)   # do this later


# need timer 3 for the motors

# pinB4 = pyb.Pin (pyb.Pin.cpu.B4, pyb.Pin.OUT_PP)                # pin intialized, look in the tim3Ch1 code, we just set the pin directly there
tim3 = pyb.Timer(3, freq = 20000)                                 # 3 is the timer number, freq is the count of the timer
tim3Ch1 = tim3.channel(1, pyb.Timer.PWM, pin = pyb.Pin.cpu.B4)    # creates a variable that accesses a timer channel. Channel is 1, uses PWM, says and iniatlizes which pin to use
tim3Ch1.pulse_width_percent(100)                                    # gives PWM as zero so motor can move in a certain direction


print('pin B4 activated, channel 1 = 0')

#pinB5 = pyb.Pin (pyb.Pin.cpu.B5, pyb.Pin.OUT_PP)              # pin intialized
#tim3 = pyb.Timer(3, freq = 20000)
tim3Ch2 = tim3.channel(2, pyb.Timer.PWM, pin = pyb.Pin.cpu.B5)
tim3Ch2.pulse_width_percent(0)

print('pin B5 activated, channel 2 = 100')



# do these later
#IN3 = pyb.Pin (pyb.Pin.cpu.B0)
#IN4 = pyb.Pin (pyb.Pin.cpu.B1)







"""
class DRV8847:
    ''' @brief A motor driver class for the DRV8847 from TI.
        @details
    '''
    
    
    
    def __init__ (self):
        ''' @brief
        '''
        pass
    
    def enable (self):
        ''' @brief
        '''
        pass
    
    def disable (self):
        ''' @brief
        '''
        pass
    
    def fault_cb (self, IRQ_src):
        ''' @brief 
        '''
        pass
    
    def motor (self):
        ''' @brief
        '''
        return Motor()
    
class Motor :
    ''' @brief
    '''
    
    def __init__ (self):
        ''' @brief
        '''
        pass
    
    def set_duty (self, duty):
        '''@brief
        '''
        pass
    
if __name__ == '__main__':
    
    
    
    motor_drv = DRV8847()
    motor_1 = motor_drv.motor()
    motor_2 = motor_drv.motor()
    
    
    
    
    motor_drv.enable()
    
    
    
    motor_1.set_duty(40)
    motor_2.set_duty(60)
    
"""
